# Bobbapp: infrastructure

This repository uses Terraform to setup and configure Kubernetes cluster for [People Service](https://gitlab.com/azizos/bobbapp) on Google Cloud (GKE).

In addition, it's being used as a GitOps repostory (managed by Flux operator running in Kubernetes) that watches changes in Kubernetes manifests (`helm` directory).